---
layout: post
title: "Gitee 企业版更新：新视图 & 新模块，新增“内源”管理功能"
---

<p>Gitee 企业版提供「企业」与「项目」两种视图，其中，「企业」视图呈现工作台以及企业共同资源，而「项目」聚焦具体项目的资源，提供更多可视化工具帮助用户高效协作。</p>

<p>Gitee 团队近期对「企业」视图进行了全新改版，更加精简优美的同时，增加了实用的「内源」和「统计」模块。</p>

<p>一起看看此次改版的三大亮点：</p>

<h3>工作台</h3>

<p><img height="378" src="https://static.oschina.net/uploads/space/2020/0302/082045_poaL_3820517.png" width="700" /></p>

<p>改版后的「工作台」界面更加明确地展示了&ldquo;我参与的&rdquo;项目、任务、Pull Request、仓库。所有&ldquo;与我相关&rdquo;的内容都可以一键直达。与旧版视图相比，最主要的变化是增加了【我参与的项目】，项目基本情况清晰可见，能够帮助用户以项目的维度划分工作内容。用户可以通过点击项目卡片，进入【项目视图】对项目进行更加精细化的管理。 项目卡片中可直接看到任务的进展变化 ：</p>

<p><img height="177" src="https://static.oschina.net/uploads/space/2020/0302/082056_DxMW_3820517.png" width="500" /></p>

<p>工作台页面右上方以日、周、月的维度记录代码量及任务量，用数字直观地显示工作进度，有利于企业成员把控自己的工作节奏。被大家广泛喜爱的「周报」功能，也可以在工作台快速进入。</p>

<h3>内源</h3>

<p>内源（InnerSource）这个术语是蒂姆&middot;奥莱利（Tim O&rsquo;Reilly）在 2000 年创造的，旨在使用开源软件开发最佳实践在组织内建立类似开源的文化。该组织可能仍会开发专有软件，但会在内部开放其开发。<a href="https://gitee.com/InnerSource" target="_blank">点击这里，了解更多内部开源文化</a>。</p>

<p>&ldquo;内源&rdquo;注重内部的技术协同，以帮助减少代码的重复开发，提升项目推进效率；同时打破壁垒，营造开放的技术氛围和代码文化。</p>

<p><img height="453" src="https://static.oschina.net/uploads/space/2020/0302/082125_rSXL_3820517.png" width="700" /></p>

<p>在 Gitee 企业版的「内源」模块，所有企业内部成员可以直接访问内部开源的仓库。「开源统计」与「开源之星」记录了参与开源的成员的工作产出，激励企业成员积极参与内部开源，贡献代码。</p>

<h3>统计</h3>

<p>研发工作的量化统计一直是个难点，Gitee 企业版在「统计」模块提供了成员、项目、仓库等维度的统计，为企业衡量研发效能、把控项目进度提供更多参考。</p>

<p><br />
以成员维度为例，通过「成员工作统计」，可以查看某个团队或具体成员在指定时间范围之内代码行数、完成任务情况、代码提交次数等信息，还可一键打印结果，是不是很懂你？</p>

<p><img height="342" src="https://static.oschina.net/uploads/space/2020/0302/082145_fB6k_3820517.png" width="700" /></p>

<p>更多惊喜，等你发现：<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>

<p>PS. Gitee 上线了全新分支权限功能，详情：<a href="https://blog.gitee.com/2020/02/27/protected-branches/">https://blog.gitee.com/2020/02/27/protected-branches/</a>，速去体验。</p>
