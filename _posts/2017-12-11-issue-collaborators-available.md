---
layout: post
title: "码云本周更新，新增任务协作者功能"
---

1. 项目、企业任务新增协作者功能，可指派多个协作者
![输入图片说明](https://gitee.com/uploads/images/2017/1210/200956_64253196_62561.png "屏幕截图.png")
2. 增加码云项目右上角悬浮挂件参见 [详情](https://gitee.com/git-osc)
![输入图片说明](https://gitee.com/uploads/images/2017/1210/201023_c52d9f33_62561.png "屏幕截图.png")
3. 企业版介绍页改版 [码云企业版](https://gitee.com/enterprises)
4. Wiki功能 放开 Home 主页的修改限制
5. 同一个项目的同一个源分支和目标分支更改为只能存在一个开启的PR
6. 企业版 PR 里程碑列表只显示开启的里程碑
7. 项目评论的子评论默认不@层主（不影响通知）
8. 新建PR项目分支的选择支持模糊匹配

赶快来 [https://gitee.com/](https://gitee.com/) 体验吧！