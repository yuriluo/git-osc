---
layout: post
title: "代码质量管理利器 Gitee Scan 发布"
---

<p>在项目开发的过程中，代码质量管理是非常重要的一个环节，项目中代码质量的高低直接影响着整个开发团队的工作效率。代码质量低会直接带来线上 Bug 频发、工作成果与工作时间不成正比、查找 Bug 困难等问题。</p>

<p>这些问题单纯的依靠开发后的人工测试是无法发现的，只能通过代码审查去发现和解决。进行代码审查时，除了严格执行代码规范、编写单元测试、必须的 Code Review、代码模块化等方法以外，有一件趁手的工具也是必不可少的。</p>

<p>Gitee 为了协助用户提高代码质量，进行完善的代码审查，推出了<strong>代码质量分析工具 Gitee Scan</strong> 。</p>

<p>依托于 Gitee 企业版的现有功能，Gitee Scan 能够通过扫描仓库内的代码，帮助开发团队找出其中的问题，尤其是一些不需要人工审查的低质量错误，帮助团队进行代码审查，从而提高代码质量。</p>

<p>Gitee Scan 可以同时从代码缺陷和代码规范两个方面对代码进行扫描，快速定位错误代码和漏洞的位置，帮助开发人员将更多精力放在解决问题而不是发现问题上。目前 Gitee Scan 支持的语言有 Java、Python、PHP、C、C++、JavaScript、Go 以及独立针对 Android 特性的扫描。&nbsp;</p>

<p>在使用 Gitee Scan 时，用户可以通过两种方式发起代码质量分析：</p>

<ul>
	<li>进入指定仓库中选择仓库分支发起全量扫描</li>
	<li>开启新建 Pull Request 增量代码自动扫描</li>
</ul>

<p>当开发人员想要对以前的代码进行错误检查时，可以选择进行全量扫描。全量扫描时，用户可以直接选择某个分支直接进行扫描操作并产出报告。报告中包含了缺陷报告及规范报告，在缺陷报告中，Gitee Scan 会通过 Bug、安全漏洞以及代码异味三个类别将问题分类，方便开发人员有针对性的修改。&nbsp;</p>

<p>如下图所示，报告中已经精准的定位到了错误代码的位置以及其可能的危险程度，开发人员直接进行修改即可。&nbsp;</p>

<p><img alt="" height="322" src="https://oscimg.oschina.net/oscnet/up-fe995738d4d4d49d0da3093a1df62704c25.png" width="700" /></p>

<p>在开发人员想要对后续产生的新代码进行错误检查时，可以选择进行增量扫描。Gitee Scan 增量扫描与全量扫描原理相同，但增量扫描更加自动化。开启增量扫描的开关后，企业内的所有分支在接受 Pull Request 前都会经过 Gitee Scan 的自动扫描并产出报告，每提交一次 Pull Request 后都会对提交的代码进行一次自动扫描。</p>

<p><img alt="" height="418" src="https://oscimg.oschina.net/oscnet/up-a155fd300c2819683d460740531321df74d.png" width="700" /></p>

<p>测试结果表明，针对同一份代码，Gitee Scan 所扫描出的漏洞数量比同类型产品 SonarQube 多出 41.7%，并且 Gitee Scan 扫描后提供了代码评级及代码规范报告，用户可以对代码质量有更直观的了解。</p>

<p><img alt="" height="295" src="https://oscimg.oschina.net/oscnet/up-783afe996038fdfde013ffd3d54acc362c0.png" width="700" /></p>

<p><img alt="" height="360" src="https://oscimg.oschina.net/oscnet/up-ca87bf56d2f3345c38a48b9c86ca703a9cf.png" width="700" /></p>

<p>在后续的更新中，Giee Scan 会增加更多语言的支持以及更加深度的扫描能力，并且后续计划增加基于 CVE 的依赖项漏洞扫描，进一步帮助用户提升研发质量和效率。</p>

<p>&nbsp;*Gitee Scan 已对 Gitee 企业版付费客户开放功能</p>

<p>了解 Gitee 企业版更多功能，请点击下面的链接：<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>
