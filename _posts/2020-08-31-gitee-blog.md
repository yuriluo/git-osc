---
layout: post
title: "【一周】Intel回应Linus怒怼 | Linux 29周年 | 美国“盯上”Gitee"
---

<p>美国知名媒体 TechCrunch 关注中国建设独立开源平台；Intel 回应 Linus 对 AVX-512 的批评；Linux 迎来 29 岁：从个人爱好到统治世界的操作系统；两个月新增 80 万行代码，Linux 内核维护为什么不会崩？</p>
