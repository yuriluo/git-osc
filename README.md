关于Gitee
-------------
>Gitee 是[开源中国社区](http://www.oschina.net)团队开发的在线代码托管平台。

Gitee 的功能
-------------
>Gitee 除了提供最基础的 Git 代码托管之外，还提供代码在线查看、在线编辑提交、历史版本查看、Fork、Pull Reuqest 、打包下载任意版本、Issue、Wiki 等方便管理、开发、协作、共享的功能，具体请查看[帮助](http://git.osc
>在您使用 Gitee 的过程中有任何意见和建议，请到此项目提交Issue，我们的开发者会在这里跟您沟通。

帮助
-------------
>在使用 Gitee 的过程中有任何不明白的问题，可以点击[帮助](http://git.oschina.net/oschina/git-osc/wikis/帮助)查看简要的帮助说明，也欢迎您到[开源中国社区](http://www.oschina.net)上提问。
>[ProGit](http://git.oschina.net/progit) 是一本权威的git书籍，[点击这里开始阅读(中文版)](http://git.oschina.net/progit)。

更新日志
-------------
>Gitee 将持续为用户提供更好的代码托管，协作开发服务，持续发布新特性，想了解 Gitee 近期的新功能可前往 [Gitee 更新日志](http://oschina.gitee.io/git-osc/) 查看。